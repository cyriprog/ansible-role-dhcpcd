ansible-role-dhcpcd
==================

A role that edit /etc/dhcpcd.conf configuration.

Requirements
------------

Role Variables
--------------

```yml
# defaults file for ansible-role-dhcpcd
dhcpcd:
  # controlgroup: wheel # Optionnal
  hostname: "" # Optionnal
  hostname_short: "" #Optionnal
  clientid: "" # Optionnal
  duid: "" # Optionnal
  persisent: yes # Boolean, default no
  options: []
  require: []
  slaac: hwaddr #hwaddr ou private
  interfaces: []
    # - name: eth0
    #   values:
    #     - dhcp
    # - name: eth1
    #   props:
    #     - static ip_address=192.168.0.10/24
    #     - static ip6_address=fd51:42f8:caae:d92e::ff/64
    #     - static routers=192.168.0.1
    #     - static domain_name_servers=192.168.0.1 8.8.8.8 fd51:42f8:caae:d92e::1
  profile: []
    # - name: static_eth0
    #   props:
    #     - static ip_address=192.168.1.23/24
    #     - static routers=192.168.1.1
    #     - static domain_name_servers=192.168.1.1
  fallback: []
    # - interface: eth0
    #   profiles:
    #       - static_eth0
```

Dependencies
------------

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - ansible-role-dhcpcd

License
-------

BSD
